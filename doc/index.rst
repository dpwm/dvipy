.. dvipy documentation master file, created by
   sphinx-quickstart2 on Fri Jul  6 18:59:36 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dvipy's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   quickstart



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

