Introduction
============

The TeX typesetting system provides excellent typesetting. When mathematical
notation is involved, it is truly unsurpassed.

TeX was written as a program, which means that using it from an external
program is difficult and usually very hacky, often relying on using several
support scripts in ways that they were never intended. Use of these scripts
often requires either putting the desired text into batches and then running
latex on these batches, which is awkward, or running the entire toolchain on
each piece of text, which is slow.

Dvipy arose from the very real need for a library that is capable of running
TeX and processing its output (the DVI file) from Python, without using any
external packages. This makes use of the -ipc flag, which is available in all
good tex distributions. The only binaries that must be callable are latex and
kpsewhich.

At present, dvipy uses LaTeX, which is much more widely known throughout the
scientific community than plain TeX.

Windows is currently not supported, due to lack of sufficient asynchronous
non-blocking pipe reads from within python. Windows is not an intended target,
but if a sufficient workaround can be proposed then it can certainly be
adopted. 


