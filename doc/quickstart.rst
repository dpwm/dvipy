Quickstart
==========

The best place to start is with the TexRunner class.

>>> import dvipy
>>> texrunner = dvipy.TexRunner()

This sets up a TexRunner with the default parameters. Let's get our first page.

>>> texrunner.page()

A page is a list of glyphs, along with size information. It is a conversion of
pretty much all the relevant information encoded in the DVI file in a form
accessible from Python. This can then be used to render text, obtain sizes.

Once the texrunner is no longer needed, it is best to close the texrunner, as
you should with files. Not doing this can stop cleanup from happening, or leave
the LaTeX program running in the background.

>>> texrunner.close()

The TexRunner is also a contextmanager, meaning that it can be used in a with
block.

>>> with dvipy.TexRunner() as tex:
...     # Do stuff here

This will automatically close the TexRunner, but may not be well suited to more
complex uses.
