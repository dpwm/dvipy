import freetype
import dvipy
from dvipy.font import kpse
import cairo
from random import random
from math import pi

Cairo = freetype.Cairo
ft_faces = {}
cr_faces = {}

freetype = freetype.Freetype()
# Note that the freetype module included here is a minimal ctypes
# wrapper around freetype and cairo.

# Create our png surface
surf = cairo.ImageSurface(cairo.FORMAT_RGB24, 640, 480)

cr = cairo.Context(surf)
# Set the background to be white
cr.set_source_rgb(1, 1, 1)
cr.paint()

cr.translate(320, 240)
cr.scale(4, 4)

cr.save()
options = cairo.FontOptions()
options.set_hint_metrics(cairo.HINT_METRICS_OFF)
options.set_hint_style(cairo.HINT_STYLE_NONE)
options.set_antialias(cairo.ANTIALIAS_GRAY)
cr.set_font_options(options)
# TexRunner
tr = dvipy.TexRunner()

for i in xrange(500):
    cr.restore()
    cr.save()
    hello = tr.page(r'$\text{DVI}^{\text{py}}$')
    w, h = hello.size
    cr.translate(320 * (0.5 - random()), 240 * (0.5 - random()))
    s = 0.5 + random()
    r = 0.5 - random()
    cr.scale(s, s)
    cr.rotate(r)
    cr.translate(-0.5 * w, 0.5 * h)
    cr.set_source_rgba(1-(0.002*i)**9, 0, 0, (0.002 * i)**8)
    for glyph in hello.page:
        filename = glyph.font.filename
        # Load the face
        ft_face = ft_faces.get(filename)

        # Create the freetype face if it doesn't exist
        if not ft_face:
            ft_face = freetype.new_face(kpse(filename))
            ft_faces[filename] = ft_face

        # We could have been given a glyph or a char by TeX. Glyphs will be
        # strings!
        if isinstance(glyph.char, str):
            i = ft_face.get_name_index(glyph.char)
        else:
            # This is necessary for Cairo to render some glyphs!
            ft_face.select_charmap(freetype.ADOBE_CUSTOM)
            i = ft_face.get_char_index(glyph.char)
            ft_face.select_charmap(freetype.UNICODE)

        # Create the cairo face if it doesn't exist
        cr_face = cr_faces.get(filename)
        if not cr_face:
            cr_face = Cairo.cr_face_from_ft_face(ft_face)
            cr_faces[filename] = cr_face

        Cairo.set_font_face(cr, cr_face)
        cr.set_font_options(options)

        cr.set_font_size(glyph.font.size)
        cr.show_glyphs([(i, glyph.x, glyph.y)])
    

surf.write_to_png('test.png')
